# PRZYKLADOWY NAGLOWEK


Paragraf jeden

Paragraf dwa

Paragraf trzy

Paragraf cztery


~~przekreslony tekst~~

**A tutaj tekst pogrubiony**

*No a tu w kursywie*

>Cytat
>>jakis
>>>inny


Lista numeryczna zagniezdzona 

1. Element listy
2. Element listy
3. Element listy
	1. podelement listy
	2. drugi podelement


Lista zagniezdzona nienumeryczna 

- Element
- Element kolejny
- Kolejny
	+ Podelement
	+ Podelement
	+ Podelement


Blok kodu:

```py
def jakas_funkcja():
	return True

jakas_funkcja()
```


Tutaj przyklad kodu `print("Hello world")`



![Alt text](https://gitlab.com/jzolkiewski/projekt-1/-/blob/main/kitten.png)



